Config {
    position = TopP 0 0,
    font = "xft:xos4 terminus:size=12:normal:antialias=true:hinting=true",
    bgColor = "#000000",
    fgColor = "#ffffff",
    lowerOnStart = True,
    overrideRedirect = False,
    allDesktops = True,
    persistent = True,
    commands = [
        Run MultiCpu ["-t","Cpu:<total0> <total1> <total2> <total3> <total4> <total5> <total6> <total7>","-L","30","-H","60","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC","-w","3"] 10,
        Run Memory ["-t","Mem: <usedratio>%","-H","8192","-L","4096","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Network "wlo1" ["-t","Net: <rx>, <tx>","-H","200","-L","10","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Date "%a %_d %b %Y %H:%M" "date" 10,
        Run Com "/bin/bash" ["-c", "~/.xmonad/xmonad-volume.sh"] "volumelevel" 10,
        Run Kbd [("us" , "<fc=#FFFFFF>us</fc>"),("gr", "<fc=#FFFFFF>gr</fc>")],
        Run StdinReader
    ],
    sepChar = "%",
    alignSep = "}{",
    template = "%StdinReader% }{ %multicpu% | %memory% | %wlo1% | %kbd%  | Vol: <fc=#b2b2ff>%volumelevel%</fc> | <fc=#FFFFCC>%date%</fc>"
}
